from django.shortcuts import render, get_object_or_404
from .models import TodoList

def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {'todo_lists': todo_lists}
    return render(request, 'todos/todo_list.html', context)

#def todo_list_detail_view(request, id):
    #todolist = TodoList.objects.get(id=id)
    #return render(request, 'todos/todo_list_detail.html', {'todolist': todolist})

def todo_list_detail_view(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    todo_items = todolist.items.all()
    return render(request, 'todos/todo_list_detail.html', {'todolist': todolist, 'todo_items': todo_items})
